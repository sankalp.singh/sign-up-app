import "./App.css";
import React, { Component } from "react";
import Signup from "./Components/Signup";

class App extends Component {
	render() {
		return (
			<div className="App">
				<h1>Create your Account Here!</h1>
				<Signup />
			</div>
		);
	}
}
export default App;
