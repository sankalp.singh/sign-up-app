import React, { Component } from "react";
import "./signup.css";

class Signup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstName: "",
			lastName: "",
			email: "",
			password: "",
			confirmPassword: "",
			firstNameError: "",
			lastNameError: "",
			emailError: "",
			passwordError: "",
			confirmPasswordError: "",
			successMsg: "",
		};
	}

	firstNameHandler = (event) => {
		this.setState({ firstName: event.target.value });
	};

	lastNameHandler = (event) => {
		this.setState({ lastName: event.target.value });
	};

	emailHandler = (event) => {
		this.setState({ email: event.target.value });
	};

	passwordHandler = (event) => {
		this.setState({ password: event.target.value });
	};

	confirmPasswordHandler = (event) => {
		this.setState({ confirmPassword: event.target.value });
	};

	submitHandler = (event) => {
		const hasNumber = /d/;
		const emailRegex =
			/^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
		const strongRegex =
			/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/;

		let firstNameMsg = "";
		let lastNameMsg = "";
		let emailMsg = "";
		let passwordMsg = "";
		let confirmPasswordMsg = "";

		if (
			hasNumber.test(this.state.firstName) ||
			this.state.firstName === "" ||
			this.state.firstName === null
		) {
			firstNameMsg = "Invalid name";
			// this.setState({
			// 	firstNameError: "Invalid Name",
			// });
		}
		if (
			hasNumber.test(this.state.lastName) ||
			this.state.lastName === "" ||
			this.state.lastName === null
		) {
			lastNameMsg = "Invalid name";
			// this.setState({
			// 	lastNameError: "Invalid Name",
			// });
		}
		if (
			this.state.email === "" ||
			this.state.email === null ||
			!emailRegex.test(this.state.email)
		) {
			emailMsg = "Invalid email";
			// this.setState({
			// 	emailError: "Invalid email address",
			// });
		}
		if (
			this.state.password === "" ||
			this.state.password === null ||
			!strongRegex.test(this.state.password)
		) {
			passwordMsg = "Invalid password";
			// this.setState({
			// 	passwordError: "Invalid password",
			// });
		}
		if (this.state.password !== this.state.confirmPassword) {
			confirmPasswordMsg = "Password doesnt match";
			// this.setState({
			// 	confirmPasswordError: "Password don't match!",
			// });
		}
		if (
			firstNameMsg === "" &&
			lastNameMsg === "" &&
			emailMsg === "" &&
			passwordMsg === "" &&
			confirmPasswordMsg === ""
		) {
			this.setState({
				firstName: "",
				lastName: "",
				email: "",
				password: "",
				confirmPassword: "",
				firstNameError: "",
				lastNameError: "",
				emailError: "",
				passwordError: "",
				confirmPasswordError: "",
				successMsg: "Account Created Successfully",
			});
		} else {
			this.setState({
				firstNameError: firstNameMsg,
				lastNameError: lastNameMsg,
				emailError: emailMsg,
				passwordError: passwordMsg,
				confirmPasswordError: confirmPasswordMsg,
				successMsg: "",
			});
		}
	};

	render() {
		return (
			<div className="signup-Form">
				<form>
					<fieldset>
						<legend>First Name</legend>
						<input
							type="text"
							value={this.state.firstName}
							onChange={this.firstNameHandler}></input>
						<div className="error">{this.state.firstNameError}</div>
					</fieldset>
					<fieldset>
						<legend>Last Name</legend>
						<input
							type="text"
							value={this.state.lastName}
							onChange={this.lastNameHandler}></input>
						<div className="error">{this.state.lastNameError}</div>
					</fieldset>
					<fieldset>
						<legend>E-mail</legend>
						<input
							type="text"
							value={this.state.email}
							onChange={this.emailHandler}></input>
						<div className="error">{this.state.emailError}</div>
					</fieldset>
					<fieldset>
						<legend>Password</legend>
						<input
							type="password"
							value={this.state.password}
							onChange={this.passwordHandler}></input>
						<div className="error">{this.state.passwordError}</div>
					</fieldset>
					<fieldset>
						<legend>Confirm Password</legend>
						<input
							type="password"
							value={this.state.confirmPassword}
							onChange={this.confirmPasswordHandler}></input>
						<div className="error">{this.state.confirmPasswordError}</div>
					</fieldset>
				</form>
				<div className="success">{this.state.successMsg}</div>
				<button onClick={this.submitHandler}>Sign up</button>
			</div>
		);
	}
}
export default Signup;
